package Threads.BattleField;

import Data.GameData;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.MediaPlayer;

public class CountDownSFx implements Runnable {

    GameData GD;

    public CountDownSFx(GameData GD) {
        this.GD = GD;

    }
    MediaPlayer menuMediaPlayer;

    @Override
    public void run() {

        GD.menuMediaPlayer.setVolume((double) 0.75);
        try {
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_10);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_9);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_8);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_7);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_6);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_5);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_4);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_3);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_2);
            menuMediaPlayer.play();
            Thread.sleep(1000);
            menuMediaPlayer = new MediaPlayer(GD.SR.CD_1);
            menuMediaPlayer.play();

        } catch (InterruptedException ex) {
            Logger.getLogger(CountDownSFx.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
