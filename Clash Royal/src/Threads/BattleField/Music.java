package Threads.BattleField;

import Data.GameData;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.media.MediaPlayer;

public class Music implements Runnable {

    GameData GD;

    public Music(GameData GD) {
        this.GD = GD;
    }

    @Override
    public void run() {

        Random rndm = new Random();

        switch (rndm.nextInt(3)) {
            case 0:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.BattleMusic_1);
                break;
            case 1:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.BattleMusic_2);
                break;
            case 2:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.BattleMusic_3);
                break;

        }
        GD.menuMediaPlayer.play();
        try {
            Thread.sleep(120000);
        } catch (InterruptedException ex) {
            Logger.getLogger(Music.class.getName()).log(Level.SEVERE, null, ex);
        }
        GD.menuMediaPlayer = new MediaPlayer(GD.SR.Sudden_death);
        GD.menuMediaPlayer.play();
    }

}
