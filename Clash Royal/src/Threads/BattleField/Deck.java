package Threads.BattleField;

import Data.GameData;
import GUI.BattleFieldController;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;

public class Deck implements Runnable {

    GameData GD;
    BattleFieldController BFC;

    public Deck(GameData GD, BattleFieldController BFC) {
        this.GD = GD;
        this.BFC = BFC;

    }

    @Override
    public void run() {

        while (true) {

            Platform.runLater(() -> {
                BFC.cardSlot1.setImage(GD.deckTroopSaver_1.get(0));
                BFC.cardSlot2.setImage(GD.deckTroopSaver_1.get(1));
                BFC.cardSlot3.setImage(GD.deckTroopSaver_1.get(2));
                BFC.cardSlot4.setImage(GD.deckTroopSaver_1.get(3));
                BFC.nextCard.setImage(GD.deckTroopSaver_1.get(4));
            });
            try {
                Thread.sleep(200);
            } catch (InterruptedException ex) {
                Logger.getLogger(Deck.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

}
