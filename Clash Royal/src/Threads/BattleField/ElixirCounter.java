package Threads.BattleField;

import Data.GameData;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.image.ImageView;

public class ElixirCounter implements Runnable {

    final int MAX = 10;
    private GameData GD;
    private Label elixirCounterLbl;
    private ImageView elixirProgressBar;

    public ElixirCounter(GameData GD, Label elixirCounter, ImageView elixirProgressBar) {
        this.GD = GD;
        this.elixirCounterLbl = elixirCounter;
        this.elixirProgressBar = elixirProgressBar;

    }

    @Override
    public void run() {

        while (true) {

            Platform.runLater(new Runnable() {

                @Override
                public void run() {
                    elixirCounterLbl.setText(GD.getElixirCount() + "");
                    switch (GD.getElixirCount()) {
                        case 0:
                            elixirProgressBar.setImage(GD.IR.Elixir_0);
                            break;
                        case 1:
                            elixirProgressBar.setImage(GD.IR.Elixir_1);
                            break;
                        case 2:
                            elixirProgressBar.setImage(GD.IR.Elixir_2);
                            break;
                        case 3:
                            elixirProgressBar.setImage(GD.IR.Elixir_3);
                            break;
                        case 4:
                            elixirProgressBar.setImage(GD.IR.Elixir_4);
                            break;
                        case 5:
                            elixirProgressBar.setImage(GD.IR.Elixir_5);
                            break;
                        case 6:
                            elixirProgressBar.setImage(GD.IR.Elixir_6);
                            break;
                        case 7:
                            elixirProgressBar.setImage(GD.IR.Elixir_7);
                            break;
                        case 8:
                            elixirProgressBar.setImage(GD.IR.Elixir_8);
                            break;
                        case 9:
                            elixirProgressBar.setImage(GD.IR.Elixir_9);
                            break;
                        case 10:
                            elixirProgressBar.setImage(GD.IR.Elixir_10);
                            elixirCounterLbl.setStyle(
                                    "-fx-translate-x: 64;"
                                    + "-fx-translate-y: 973;"
                                    + "-fx-font-size : 24px;");
                            break;
                    }
                }
            });
            try {
                Thread.sleep(2000);
            } catch (InterruptedException ex) {
                Logger.getLogger(ElixirCounter.class.getName()).log(Level.SEVERE, null, ex);
            }
            GD.changeElixirCount(1);
        }
    }

}
