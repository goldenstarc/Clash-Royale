package Threads.BattleField;

import Data.GameData;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.scene.control.Label;

public class Time implements Runnable {

    GameData GD;
    Label timeLbl;
    Label earnedCup1Lbl;
    Label earnedCup2Lbl;

    CountDownSFx countDownSFxCL;
    Thread countDownSFxTH;

    public Time(GameData GD, Label timeLbl, Label earnedCup1Lbl, Label earnedCup2Lbl) {
        this.timeLbl = timeLbl;
        this.earnedCup1Lbl = earnedCup1Lbl;
        this.earnedCup2Lbl = earnedCup2Lbl;
        this.GD = GD;

    }

    @Override
    public void run() {

        int CupA = 0;
        int CupB = 0;

        while (GD.time >= 0) {

            if (CupA != GD.earnedCup1) {
                CupA = GD.earnedCup1;
                final int temp = CupA;
                Platform.runLater(() -> {
                    earnedCup1Lbl.setText(temp + "");
                });
            } else if (CupB != GD.earnedCup2) {
                CupB = GD.earnedCup2;
                final int temp = CupB;
                Platform.runLater(() -> {
                    earnedCup1Lbl.setText(temp + "");
                });
            }

            if (GD.time == 10) {
                countDownSFxCL = new CountDownSFx(GD);
                countDownSFxTH = new Thread(countDownSFxCL);
                countDownSFxTH.start();
            }
            int min = GD.time / 60;
            int sec = GD.time - min * 60;
            String temp = String.format("%d:%02d", min, sec);
            Platform.runLater(() -> {
                timeLbl.setText(temp);
            });

            try {
                Thread.sleep(1000);
            } catch (InterruptedException ex) {
                Logger.getLogger(Time.class.getName()).log(Level.SEVERE, null, ex);
            }
            GD.time--;
        }
    }
}
