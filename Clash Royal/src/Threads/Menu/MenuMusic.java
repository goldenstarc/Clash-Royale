package Threads.Menu;

import Data.GameData;
import java.util.Random;
import javafx.scene.media.MediaPlayer;

public class MenuMusic implements Runnable {

    GameData GD;

    public MenuMusic(GameData GD) {
        this.GD = GD;

    }

    @Override
    public void run() {

        Random rndm = new Random();

            switch (rndm.nextInt(3)) {
                case 0:
                    GD.menuMediaPlayer = new MediaPlayer(GD.SR.MenuMusic_1);
                    break;
                case 1:
                    GD.menuMediaPlayer = new MediaPlayer(GD.SR.MenuMusic_2);
                    break;
                case 2:
                    GD.menuMediaPlayer = new MediaPlayer(GD.SR.MenuMusic_3);
                    break;

            }
            GD.menuMediaPlayer.play();

    }
}
