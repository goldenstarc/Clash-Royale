package Threads.Menu;

import Data.GameData;
import java.util.Random;
import javafx.scene.media.MediaPlayer;

public class BattleStartMusic implements Runnable {

    GameData GD;

    public BattleStartMusic(GameData GD) {
        this.GD = GD;

    }

    @Override
    public void run() {

        Random rndm = new Random();

        switch (rndm.nextInt(9)) {
            case 0:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.legendary_arena_jingle_01);
                break;
            case 1:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.tut_jing_01);
                break;
            case 2:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.jungle_arena_jingle_01);
                break;
            case 3:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.arctic_arena_jingle);
                break;
            case 4:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.barbarian_jingle_01);
                break;
            case 5:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.bone_pit_arena_jingle_01);
                break;
            case 6:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.dark_arena_jingle_02);
                break;
            case 7:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.extratime_jingle_02);
                break;
            case 8:
                GD.menuMediaPlayer = new MediaPlayer(GD.SR.goblin_arena_jingle_01);
                break;

        }
        GD.menuMediaPlayer.play();

    }
}
