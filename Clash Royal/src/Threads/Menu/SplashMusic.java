package Threads.Menu;

import Data.GameData;
import javafx.scene.media.MediaPlayer;

public class SplashMusic implements Runnable {

    GameData GD;

    public SplashMusic(GameData GD) {
        this.GD = GD;

    }

    @Override
    public void run() {
        
        GD.menuMediaPlayer = new MediaPlayer(GD.SR.SplashMusic);
        GD.menuMediaPlayer.play();
    }
}
