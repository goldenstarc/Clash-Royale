package Card.Building;

import Card.BaseCard;
import Data.GameData;

public abstract class Buildings extends BaseCard {

    private int hitpoints;
    private int deployTime;

    private int lifeTime;

    //-------------contractor--------------------
    public Buildings(int player, String cardName, String rarity, int costElixir, int hitpoints, int lifeTime, int deployTime, int x, int y, GameData GD) {
        super(player, cardName, costElixir, rarity, "Buildings", x, y, GD);
        setHitpoints(hitpoints);
        setLifeTime(lifeTime);
        setDeployTime(deployTime);

    }

    //-----------------set-----------------------
    private void setHitpoints(int hitpoints) {
        this.hitpoints = hitpoints;
    }

    private void setDeployTime(int deployTime) {
        this.deployTime = deployTime;
    }

    private void setLifeTime(int lifeTime) {
        this.lifeTime = lifeTime;
    }

    //-----------------get-----------------------
    public int getDeployTime() {
        return deployTime;
    }

    public int getLifeTime() {
        return lifeTime;
    }

    public int getHitpoints() {
        return hitpoints;
    }

}
