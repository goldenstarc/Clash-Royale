package Card.Building.Maker;

import Data.GameData;
import javafx.scene.image.Image;

public class ElixirCollector extends MakerBuilding {

    float productionSpeed;

    public ElixirCollector(int player, int x, int y, GameData GD) {
        super(player, "ElixirCollector", "Rare", 6, 1020, 70, 1, x, y, GD);
        setProductionSpeed((float) 8.5);
        setCardImage();
    }

    private void setProductionSpeed(float productionSpeed) {
        this.productionSpeed = productionSpeed;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCardImage() {
        cardImage = GD.CR.ElixirCollector_Card;
    }

    @Override
    public void setGif() {
        if (player == 1) {
            Gif = new Image("resources\\Image\\Troop\\Gif\\ElixirCollector\\1.gif");
        } else if (player == 2) {
            Gif = new Image("resources\\Image\\Troop\\Gif\\ElixirCollector\\2.gif");

        }
    }

}
