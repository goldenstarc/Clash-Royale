package Card.Building.Maker;

import Card.Building.Buildings;
import Data.GameData;
import javafx.scene.image.Image;

public abstract class MakerBuilding extends Buildings implements MakerGif {

    private int spownSpeed;

    public Image Gif;

    public MakerBuilding(int player, String cardName, String rarity, int costElixir, int hitpoints, int lifeTime, int deployTime, int x, int y, GameData GD) {

        super(player, cardName, rarity, costElixir, hitpoints, lifeTime, deployTime, x, y, GD);
    }

}
