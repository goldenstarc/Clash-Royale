/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Card.Building.Defensive;

import Card.Building.Buildings;
import Data.GameData;

/**
 *
 * @author golde
 */
public abstract class defensiveBuilding extends Buildings {

    private int hitpoints;
    private int damagePerSecond;
    private int areaDamage;
    private final int hitSpeed = 21;
    private final String target = "bnm";
    private final int range = 12;
    private final int deployTime = 12;

    public defensiveBuilding(int player, String cardName, String rarity, int costElixir, int hitpoints, int lifeTime, int deployTime, int x, int y, GameData GD) {

        super(player, cardName, rarity, costElixir, hitpoints, lifeTime, deployTime, x, y, GD);
    }

}
