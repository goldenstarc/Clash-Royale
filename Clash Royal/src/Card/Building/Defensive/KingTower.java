package Card.Building.Defensive;

import Card.BaseCard;
import Data.GameData;
import Loader.TroopImage.CardResource;
import javafx.scene.image.Image;

public class KingTower extends BaseCard {

    private int hitpoints = 4392;
    private float range = 7;
    private float hitSpeed = 1;
    private int damage = 91;

    public KingTower(int player, int x, int y, GameData GD) {
        super(player, "KingTower", 4392, null, "Buildings", x, y, GD);
    }

    public void decreaseHealth(int value) {

        hitpoints -= value;

        if (hitpoints <= 0) {
            Utilities.End.PlayerLost(1);
        }
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCardImage() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
