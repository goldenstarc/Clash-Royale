package Card.Troop.SingleHit;

import Card.Troop.BaseTroop;
import Data.GameData;

public class MinionHorde extends BaseTroop implements SingleHit {

    private int damage;

    public MinionHorde(int player, int x, int y, GameData GD) {
        super(player, "MinionHorde", "Common", 5, "Air", 209, 93, (float) 1, "Air & Ground", "Fast", 2, 1, x, y, GD);
        setDamage(93);
        setCardImage();
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setGif() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCardImage() {
        cardImage = GD.CR.MinionHorde_Card;
    }

}
