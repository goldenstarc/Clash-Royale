package Card.Troop.SingleHit;

import Card.Troop.BaseTroop;
import Data.GameData;

public class Miner extends BaseTroop implements SingleHit {

    private int damage;

    public Miner(int player, int x, int y, GameData GD) {

        super(player, "Miner", "Legendary", 3, "Ground", 1000, 133, (float) 1.2, "Ground", "Fast", 0, 1, x, y, GD);
        setDamage(160);
        ;
        setCardImage();
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void run() {
    }

    @Override
    public void setGif() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCardImage() {
        cardImage = GD.CR.Miner_Card;
    }

}
