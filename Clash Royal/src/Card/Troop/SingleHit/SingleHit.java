package Card.Troop.SingleHit;

public interface SingleHit {

    void setDamage(int damage);

    int getDamage();

}
