package Card.Troop.SingleHit;

import Card.Troop.Util;
import Card.Troop.BaseTroop;
import Data.GameData;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class Balloon extends BaseTroop implements SingleHit {

    private int damage;

    public Balloon(int player, int x, int y, GameData GD) {

        super(player, "Ballon", "Epic", 5, "Air", 1270, 242, (float) 5, "Buildings", "Medium", 0, 1, x, y, GD);
        setDamage(726);
        setCardImage();
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void run() {
        Util util = new Util(this);
        img = new ImageView(MU);
        Platform.runLater(() -> {
            GD.pane.getChildren().add(img);
            img.setLayoutX(x);
            img.setLayoutY(y);

        });
    }

    @Override
    public void setGif() {
        if (player == 1) {
            MU = new Image("resources\\Image\\Troop\\Movement\\Balloon\\1.png");
            MD = new Image("resources\\Image\\Troop\\Movement\\Balloon\\1.png");
            MR = new Image("resources\\Image\\Troop\\Movement\\Balloon\\1.png");
            ML = new Image("resources\\Image\\Troop\\Movement\\Balloon\\1.png");
        } else if (player == 2) {
            MU = new Image("resources\\Image\\Troop\\Movement\\Balloon\\2.png");
            MD = new Image("resources\\Image\\Troop\\Movement\\Balloon\\2.png");
            MR = new Image("resources\\Image\\Troop\\Movement\\Balloon\\2.png");
            ML = new Image("resources\\Image\\Troop\\Movement\\Balloon\\2.png");
        }
    }

    @Override
    public void setCardImage() {
        cardImage = GD.CR.Balloon_Card;
    }

}
