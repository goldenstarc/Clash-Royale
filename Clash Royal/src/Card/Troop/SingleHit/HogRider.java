package Card.Troop.SingleHit;

import Card.Troop.Util;
import Card.Troop.BaseTroop;
import Data.GameData;
import Loader.TroopImage.CardResource;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class HogRider extends BaseTroop implements SingleHit {

    private int damage;

    public HogRider(int player, int x, int y, GameData GD) {

        super(player, "HogRider", "Rare", 4, "Ground", 1408, 176, (float) 1.5, "Buildings", "Very Fast", 0, 1, x, y, GD);
        setDamage(264);
        setCardImage();
    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;

    }

    @Override
    public void run() {

        Util util = new Util(this);
        img = new ImageView(MU);
        Platform.runLater(() -> {
            GD.pane.getChildren().add(img);
            img.setLayoutX(x);
            img.setLayoutY(y);
            System.out.println(img.getX());
        });
    }

    @Override
    public void setGif() {
        if (player == 1) {
            MU = new Image("resources\\Image\\Troop\\Movement\\HogRider\\Up\\1.gif");
            MD = new Image("resources\\Image\\Troop\\Movement\\HogRider\\Down\\1.gif");
            MR = new Image("resources\\Image\\Troop\\Movement\\HogRider\\Right\\1.gif");
            ML = new Image("resources\\Image\\Troop\\Movement\\HogRider\\Left\\1.gif");
        } else if (player == 2) {
            MU = new Image("resources\\Image\\Troop\\Movement\\HogRider\\Up\\2.gif");
            MD = new Image("resources\\Image\\Troop\\Movement\\HogRider\\Down\\2.gif");
            MR = new Image("resources\\Image\\Troop\\Movement\\HogRider\\Right\\2.gif");
            ML = new Image("resources\\Image\\Troop\\Movement\\HogRider\\Left\\2.gif");
        }
    }

    @Override
    public void setCardImage() {
        cardImage = GD.CR.HogRider_Card;
    }
}
