package Card.Troop.SingleHit;

import Card.Troop.Util;
import Card.Troop.BaseTroop;
import Data.GameData;
import javafx.application.Platform;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class RoyalGiant extends BaseTroop implements SingleHit {

    private int damage;

    public RoyalGiant(int player, int x, int y, GameData GD) {

        super(player, "RoyalGiant", "Common", 6, "Ground", 2796, 102, (float) 1.7, "Buildings", "Slow", (float) 6.5, 1, x, y, GD);
        setDamage(174);
        setCardImage();

    }

    @Override
    public void setDamage(int damage) {
        this.damage = damage;
    }

    @Override
    public int getDamage() {
        return damage;
    }

    @Override
    public void run() {

        Util util = new Util(this);
        img = new ImageView();
        img.setImage(MU);

        Platform.runLater(() -> {
            GD.pane.getChildren().add(img);
            img.setTranslateX(x);
            img.setTranslateY(y);
            System.out.println(img);

        });

        while (this.getHitpoints() > 0) {
            if (util.checkField() == 1) {
            } else if (util.checkField() == 2) {
            }
        }
        this.stat = false;
    }

    @Override
    public void setGif() {

        if (player == 1) {
            MU = new Image("resources\\Image\\Troop\\Movement\\RoyalGiant\\Up\\1.gif");
            MD = new Image("resources\\Image\\Troop\\Movement\\RoyalGiant\\Down\\1.gif");
            MR = new Image("resources\\Image\\Troop\\Movement\\RoyalGiant\\Right\\1.gif");
            ML = new Image("resources\\Image\\Troop\\Movement\\RoyalGiant\\Left\\1.gif");
        } else if (player == 2) {
            MU = new Image("resources\\Image\\Troop\\Movement\\RoyalGiant\\Up\\2.gif");
            MD = new Image("resources\\Image\\Troop\\Movement\\RoyalGiant\\Down\\2.gif");
            MR = new Image("resources\\Image\\Troop\\Movement\\RoyalGiant\\Right\\2.gif");
            ML = new Image("resources\\Image\\Troop\\Movement\\RoyalGiant\\Left\\2.gif");
        }
    }

    @Override
    public void setCardImage() {
        cardImage = GD.CR.RoyalGiant_Card;
    }
}
