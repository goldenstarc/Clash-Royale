package Card.Troop;

import Card.BaseCard;
import Data.GameData;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public abstract class BaseTroop extends BaseCard implements TroopGif {

    private int hitpoints;
    private String position;
    private int damagePerSec;
    private float hitSpeed;
    private String target;
    private String speed;
    private float range;
    private int deployTime;
    public ImageView img;
    public Image MU;
    public Image MD;
    public Image MR;
    public Image ML;
    public Image AU;
    public Image AD;
    public Image AR;
    public Image AL;

    public BaseTroop(int player, String cardName, String rarity, int costElixir,
            String position, int hitpoints, int damagePerSec,
            float hitSpeed, String target, String speed,
            float range, int deployTime, int x, int y, GameData GD) {

        super(player, cardName, costElixir, rarity, "Troop", x, y, GD);
        setpPsition(position);
        setHitpoints(hitpoints);
        setDamagePerSec(damagePerSec);
        setHitSpeed(hitSpeed);
        setTarget(target);
        setSpeed(speed);
        setRange(range);
        setDeployTime(deployTime);

    }

    //  ***************************************************************************************************/
    //                                                Set
    // ****************************************************************************************************/
    private void setpPsition(String position) {
        this.position = position;
    }

    private void setHitpoints(int hitpoints) {

        this.hitpoints = hitpoints;
    }

    private void setDamagePerSec(int damagePerSec) {
        this.damagePerSec = damagePerSec;
    }

    private void setHitSpeed(float hitSpeed) {
        this.hitSpeed = hitSpeed;
    }

    private void setTarget(String target) {
        this.target = target;
    }

    private void setSpeed(String speed) {
        this.speed = speed;
    }

    private void setRange(float range) {
        this.range = range;
    }

    private void setDeployTime(int deployTime) {
        this.deployTime = deployTime;
    }

    //  ***************************************************************************************************/
    //                                                  Get
    // ****************************************************************************************************/
    public String getPosition() {
        return position;
    }

    public int getHitpoints() {
        return hitpoints;
    }

    public int getDamagePerSec() {
        return damagePerSec;
    }

    public double getHitSpeed() {
        return hitSpeed;
    }

    public String getTarget() {
        return target;
    }

    public String getSpeedString() {
        return speed;
    }

    public int getSpeed() {
        switch (getSpeedString()) {
            case "Slow":
                return 5;
            case "Medium":
                return 10;
            case "Fast":
                return 20;
            default:
                break;
        }
        if (getSpeedString().equals("Very Fast")) {
            return 30;
        }
        return -5;
    }

    public float getRange() {
        return range;
    }

    public int getDeployTime() {
        return deployTime;
    }

    public Image getGif(char c) {
        switch (c) {
            case 'U':
                return MU;
            case 'D':
                return MD;
            case 'R':
                return MR;
            case 'L':
                return ML;
        }
        return null;
    }

}
