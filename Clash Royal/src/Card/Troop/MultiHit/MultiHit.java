package Card.Troop.MultiHit;

public interface MultiHit {

    void setAreaDamage(int areaDamage);

    int getAreaDamage();
}
