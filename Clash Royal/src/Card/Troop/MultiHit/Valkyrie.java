package Card.Troop.MultiHit;

import Card.Troop.BaseTroop;
import Data.GameData;
import javafx.scene.image.Image;

public class Valkyrie extends BaseTroop implements MultiHit {

    private int areaDamage;

    public Valkyrie(int player, int x, int y, GameData GD) {
        super(player, "Valkyrie", "Rare", 4, "Ground", 1408, 128, (float) 1.5, "Ground", "Medium", 0, 1, x, y, GD);
        setAreaDamage(192);
        setCardImage();
    }

    @Override
    public void setAreaDamage(int areaDamage) {
        this.areaDamage = areaDamage;
    }

    @Override
    public int getAreaDamage() {
        return areaDamage;
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCardImage() {
        cardImage = GD.CR.Valkyrie_Card;
    }

    @Override
    public void setGif() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
