package Card.Troop;

import Card.Troop.BaseTroop;
import static java.lang.Math.abs;
import static java.lang.Math.min;

public class Util {

    BaseTroop sample;
    final int BX1 = 624;
    final int BX2 = 795;
    final int BX3 = 1121;
    final int BX4 = 1298;
    final int BY = 540;

    public Util(BaseTroop sample) {
        this.sample = sample;

    }

    public int checkField() {

        if (sample.y > 530 && sample.y < 930) {
            return 1;
        } else if (sample.y > 120 && sample.y < 510) {
            return 2;
        }
        return 0;
    }

    public int findBridge() {

        int dX1 = abs(sample.x - 624);
        int dX2 = abs(sample.x - 795);
        int dX3 = abs(sample.x - 1121);
        int dX4 = abs(sample.x - 1298);

        int t1 = min(dX1, dX2);
        int t2 = min(dX3, dX4);
        int t3 = min(t1, t2);

        if (t3 == dX1) {
            return 1;
        } else if (t3 == dX2) {
            return 2;
        } else if (t3 == dX3) {
            return 3;
        } else if (t3 == dX4) {
            return 4;
        }
        return 0;
    }

    public void gotoBridge(int number) {

        if (BX1 == sample.x || BX2 == sample.x || BX3 == sample.x || BX4 == sample.x) {
            gotoY(BY);
        } else {
            switch (number) {
                case 1:
                    gotoX(BX1);
                    break;
                case 2:
                    gotoX(BX2);
                    break;
                case 3:
                    gotoX(BX3);
                    break;
                case 4:
                    gotoX(BX4);
                    break;
                default:
                    break;
            }
        }
    }

    public void find_go_Building() {

        int temp = 0;
        int des = 4000;

        for (int i = 0; i < sample.GD.troopSaver_1.size(); i++) {
            if (sample.GD.troopSaver_1.get(i).getType().equals("Buildings")) {
                if (sample.GD.troopSaver_1.get(i).stat == true) {
                    if (abs(sample.GD.troopSaver_1.get(i).x - sample.x) < des) {
                        temp = i;
                        des = abs(sample.GD.troopSaver_1.get(i).x - sample.x);
                    }
                }
            }
        }
        gotoX(sample.GD.troopSaver_1.get(temp).x);
    }

    private void gotoX(int x) {
        if (sample.x < x) {
            goRight();
        } else if (sample.x > x) {
            goLeft();
        }
    }

    private void gotoY(int y) {

        if (sample.y > y) {
            goUp();
        } else if (sample.y < y) {
            goDown();
        }
    }

    private void goRight() {
        sample.img.setImage(sample.MR);
        sample.setX(sample.x + sample.getSpeed());
    }

    private void goLeft() {
        sample.img.setImage(sample.ML);
        sample.setX(sample.x - sample.getSpeed());
    }

    private void goUp() {
        sample.img.setImage(sample.MU);
        sample.setY(sample.y - sample.getSpeed());
    }

    private void goDown() {
        sample.img.setImage(sample.MD);
        sample.setY(sample.y + sample.getSpeed());
    }

}
