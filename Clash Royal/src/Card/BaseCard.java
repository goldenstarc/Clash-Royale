package Card;

import Data.GameData;
import javafx.scene.image.Image;

public abstract class BaseCard implements Runnable, CardImages {

    public int player;
    private String cardName; // because thread class have the same field
    private String rarity;
    private String type;
    private int costElixir;
    public int x;
    public int y;
    public Boolean stat = true;
    public GameData GD;
    public Image cardImage;

    public BaseCard(int player, String cardName, int costElixir, String rarity, String type, int x, int y, GameData GD) {

        setPlayer(player);
        setCardName(cardName);
        setCostElixir(costElixir);
        setRarity(rarity);
        setType(type);
        setX(x);
        setY(y);
        setGameData(GD);

    }

    //  ***************************************************************************************************/
    //                            Set
    // ****************************************************************************************************/
    private void setCardName(String cardName) {
        this.cardName = cardName;
    }

    private void setCostElixir(int costElixir) {
        this.costElixir = costElixir;
    }

    private void setRarity(String rarity) {
        this.rarity = rarity;
    }

    private void setType(String type) {
        this.type = type;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }

    private void setGameData(GameData GD) {
        this.GD = GD;
    }

    private void setPlayer(int player) {
        this.player = player;
    }

    //  ***************************************************************************************************/
    //                            Get
    // ****************************************************************************************************/
    public String getCardName() {
        return cardName;
    }

    public int getCostElixir() {
        return costElixir;
    }

    public String getRarity() {
        return rarity;
    }

    public String getType() {
        return type;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public Image getCardImage() {
        return cardImage;
    }

}
