package Card.Spell;

import Card.BaseCard;
import Data.GameData;
import javafx.scene.image.Image;

public abstract class Spell extends BaseCard implements SpellGif {

    private double radius;
    public Image Gif;

    //-------------contractor--------------------
    public Spell(int player, String cardName, String rarity, int costElixir, float radius, int x, int y, GameData GD) {

        super(player, cardName, costElixir, rarity, "Spell", x, y, GD);
        setRadius(radius);

    }

    //-------------set--------------------
    public void setRadius(double radius) {
        this.radius = radius;
    }

    //-------------get--------------------
    public double getRadius() {
        return radius;
    }

}
