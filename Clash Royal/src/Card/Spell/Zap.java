package Card.Spell;

import Data.GameData;
import javafx.scene.image.Image;

public class Zap extends Spell {

    public Zap(int player, int x, int y, GameData GD) {
        super(player, "Zap", "Common", 2, (float) 2.5, x, y, GD);
        setCardImage();
    }

    @Override
    public void run() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void setCardImage() {
        cardImage = GD.CR.Zap_Card;
    }

    @Override
    public void setGif() {
        Gif = new Image("resources\\Image\\Troop\\Gif\\Zap");
    }

}
