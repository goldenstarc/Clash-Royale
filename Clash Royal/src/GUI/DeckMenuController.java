package GUI;

import Data.GameData;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class DeckMenuController {

    GameData GD;

    @FXML
    private ImageView background;
    @FXML
    private ImageView mainMenu;
    @FXML
    private ImageView deckMenu_Wide;
    @FXML
    private ImageView aboutMenu;
    @FXML
    private ImageView battleDeck;
    @FXML
    private ImageView woodSlot1;
    @FXML
    private ImageView woodSlot2;
    @FXML
    private ImageView woodSlot3;
    @FXML
    private ImageView woodSlot4;
    @FXML
    private ImageView woodSlot5;
    @FXML
    private ImageView usedSlot1;
    @FXML
    private ImageView usedSlot2;
    @FXML
    private ImageView usedSlot3;
    @FXML
    private ImageView usedSlot4;
    @FXML
    private ImageView usedSlot5;
    @FXML
    private ImageView usedSlot6;
    @FXML
    private ImageView usedSlot7;
    @FXML
    private ImageView usedSlot8;
    @FXML
    private ImageView notUsedSlot1;
    @FXML
    private ImageView notUsedSlot2;
    @FXML
    private ImageView notUsedSlot3;
    @FXML
    private ImageView notUsedSlot4;
    @FXML
    private ImageView notUsedSlot5;

    @FXML
    private void mainMenuClicked(MouseEvent event) {
        try {

            FXMLLoader FXL = new FXMLLoader(getClass().getResource("/GUI/MainMenu.fxml"));
            Parent root = FXL.load();
            MainMenuController MMCTR = FXL.getController();
            MMCTR.setData(GD);
            MMCTR.init();

            Scene scene = new Scene(root);

            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
            stage.setScene(scene);
            stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
            stage.setFullScreen(true);
        } catch (IOException ex) {
            Logger.getLogger(DeckMenuController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @FXML
    private void aboutMenuClicked(MouseEvent event) {
//        try {
//            String aboutCSS = MQ.Check("AboutMenu");
//            Parent root = FXMLLoader.load(getClass().getResource("/GUI/AboutMenu.fxml"));
//            Scene scene = new Scene(root);
//            scene.getStylesheets().add(getClass().getResource(aboutCSS).toExternalForm());
//            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//            stage.setScene(scene);
//            stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
//            stage.setFullScreen(true);
//
//        } catch (IOException ex) {
//            Logger.getLogger(MainMenuController.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
    }

    public void setData(GameData GD) {
        this.GD = GD;
    }

    public void init() {
        setImages();
        setCardsIcons();
    }

    private void setImages() {

        background.setImage(GD.IR.background);
        mainMenu.setImage(GD.IR.mainMenu);
        aboutMenu.setImage(GD.IR.aboutMenu);
        deckMenu_Wide.setImage(GD.IR.deckMenu_Wide);
        battleDeck.setImage(GD.IR.battleDeck);
        woodSlot1.setImage(GD.IR.woodSlot);
        woodSlot2.setImage(GD.IR.woodSlot);
        woodSlot3.setImage(GD.IR.woodSlot);
        woodSlot4.setImage(GD.IR.woodSlot);
        woodSlot5.setImage(GD.IR.woodSlot);
    }

    private void setCardsIcons() {

        usedSlot1.setImage(GD.deckTroopSaver_1.get(0));
        usedSlot2.setImage(GD.deckTroopSaver_1.get(1));
        usedSlot3.setImage(GD.deckTroopSaver_1.get(2));
        usedSlot4.setImage(GD.deckTroopSaver_1.get(3));
        usedSlot5.setImage(GD.deckTroopSaver_1.get(4));
        usedSlot6.setImage(GD.deckTroopSaver_1.get(5));
        usedSlot7.setImage(GD.deckTroopSaver_1.get(6));
        usedSlot8.setImage(GD.deckTroopSaver_1.get(7));

        notUsedSlot1.setImage(GD.CR.DarkPrince_Card);
        notUsedSlot2.setImage(GD.CR.IceWizard_Card);
        notUsedSlot3.setImage(GD.CR.LavaHound_Card);
        notUsedSlot4.setImage(GD.CR.Rage_Card);
        notUsedSlot5.setImage(GD.CR.InfernoTower_Card);
    }

}
