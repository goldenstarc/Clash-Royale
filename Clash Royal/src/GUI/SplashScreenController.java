package GUI;

import Data.GameData;
import Threads.Menu.SplashMusic;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.media.MediaPlayer;

public class SplashScreenController {

    private GameData GD;
    private Thread musicTH;
    private SplashMusic musicCL;

    @FXML
    private ImageView splashImage;

    public void init() {

        GD.menuMediaPlayer = new MediaPlayer(GD.SR.SplashMusic);
        GD.menuMediaPlayer.play();
        splashImage.setImage(GD.IR.SplashScreen);

        splashImage.setOnMouseEntered((event) -> {
            try {

                FXMLLoader FXL = new FXMLLoader(getClass().getResource("/GUI/MainMenu.fxml"));
                Parent root = FXL.load();

                GUI.MainMenuController MMCTR = FXL.getController();
                MMCTR.setData(GD);
                MMCTR.init();

                Scene scene = new Scene(root);
                GD.stage.setScene(scene);
                GD.stage.setFullScreen(true);

            } catch (IOException ex) {
                Logger.getLogger(SplashScreenController.class.getName()).log(Level.SEVERE, null, ex);
            }
        });

    }

    public void setData(GameData GD) {
        this.GD = GD;
    }

}
