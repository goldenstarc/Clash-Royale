package GUI;

import Card.BaseCard;
import Card.Building.Maker.ElixirCollector;
import Card.Spell.Zap;
import Card.Troop.MultiHit.Valkyrie;
import Card.Troop.SingleHit.Balloon;
import Card.Troop.SingleHit.HogRider;
import Card.Troop.SingleHit.Miner;
import Card.Troop.SingleHit.MinionHorde;
import Card.Troop.SingleHit.RoyalGiant;
import Threads.BattleField.*;
import Data.GameData;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.Pane;

public class BattleFieldController {

    public GameData GD;

    private Thread timeTH;
    private Time timeCL;

    private Thread musicTH;
    private Music musicCL;

    private Thread elixirCounterTH;
    private ElixirCounter elixirCounterCL;

    @FXML
    public ImageView cardSlot1;
    @FXML
    public ImageView cardSlot2;
    @FXML
    public ImageView cardSlot3;
    @FXML
    public ImageView cardSlot4;
    @FXML
    public ImageView nextCard;
    @FXML
    private ImageView background;
    @FXML
    private Label elixirCounterLbl;
    @FXML
    private ImageView Elixir_Card;
    @FXML
    private Label nextLbl;
    @FXML
    private Label timeLbl;
    @FXML
    private Label timeLeftLlb;
    @FXML
    private ImageView elixirProgressBar;
    @FXML
    private ImageView King_1;
    @FXML
    private ImageView King_2;
    @FXML
    private ImageView Princess_1A;
    @FXML
    private ImageView Princess_1B;
    @FXML
    private ImageView Princess_1C;
    @FXML
    private ImageView Princess_1D;
    @FXML
    private ImageView Princess_2A;
    @FXML
    private ImageView Princess_2B;
    @FXML
    private ImageView Princess_2C;
    @FXML
    private ImageView Princess_2D;
    @FXML
    private ImageView exit;
    @FXML
    private Label earnedCup1Lbl;
    @FXML
    private Label earnedCup2Lbl;
    @FXML
    private Pane pane;

    public void setData(GameData GD) {
        this.GD = GD;
    }

    public void init() {

        GD.setPane(pane);
        setSlot();
        setTower();

        switch (GD.getArenaNumber()) {
            case 7:
                background.setImage(GD.IR.arena7_Background);
                break;
            case 8:
                background.setImage(GD.IR.arena8_Background);
                break;
            case 9:
                background.setImage(GD.IR.arena9_Background);
                break;
            default:
                break;
        }

        pane.setOnMouseEntered((event) -> {
            if (GD.started == false) {
                elixirCounterTH.start();
                timeTH.start();
                musicTH.start();
                GD.started = true;
            }
        });

        pane.setOnMouseClicked((event) -> {
            System.out.println("X:" + event.getX());
            System.out.println("Y:" + event.getY());
            System.out.println("====++=====");
        });
        exit.setOnMouseClicked((event) -> {
            System.exit(0);
        });
        earnedCup1Lbl.setText("0");
        earnedCup2Lbl.setText("0");

        //********* Initializing Threads *********//
        elixirCounterCL = new ElixirCounter(GD, elixirCounterLbl, elixirProgressBar);
        elixirCounterTH = new Thread(elixirCounterCL);

        timeCL = new Time(GD, timeLbl, earnedCup1Lbl, earnedCup2Lbl);
        timeTH = new Thread(timeCL);

        musicCL = new Music(GD);
        musicTH = new Thread(musicCL);

        Elixir_Card.setImage(GD.IR.Elixir_Card);
        exit.setImage(GD.IR.exit);

    }

    private void setSlot() {

        //     Collections.shuffle(GD.deckTroopSaver);
        Deck deckCL = new Deck(GD, this);
        Thread deckTH = new Thread(deckCL);
        deckTH.start();
        setDrag_Drop();

    }

    private void setDrag_Drop() {

        Princess_1A.setOnDragDetected(e -> {
            Image temp = GD.CR.getDrag(GD.deckTroopSaver_1.get(0));
            String temp2 = GD.CR.getString(GD.deckTroopSaver_1.get(0));
            Dragboard db = cardSlot1.startDragAndDrop(TransferMode.COPY);
            db.setDragView(temp, e.getX(), e.getY());
            ClipboardContent cc = new ClipboardContent();
            cc.putString(temp2);
            db.setContent(cc);
        });

        cardSlot1.setOnDragDetected(e -> {
            Image temp = GD.CR.getDrag(GD.deckTroopSaver_1.get(0));
            String temp2 = GD.CR.getString(GD.deckTroopSaver_1.get(0));
            Dragboard db = cardSlot1.startDragAndDrop(TransferMode.COPY);
            db.setDragView(temp, e.getX(), e.getY());
            ClipboardContent cc = new ClipboardContent();
            cc.putString(temp2);
            db.setContent(cc);
        });

        cardSlot2.setOnDragDetected(e -> {
            Image temp = GD.CR.getDrag(GD.deckTroopSaver_1.get(1));
            String temp2 = GD.CR.getString(GD.deckTroopSaver_1.get(1));
            Dragboard db = cardSlot2.startDragAndDrop(TransferMode.COPY);
            db.setDragView(temp, e.getX(), e.getY());
            ClipboardContent cc = new ClipboardContent();
            cc.putString(temp2);
            db.setContent(cc);
        });

        cardSlot3.setOnDragDetected(e -> {

            Image temp = GD.CR.getDrag(GD.deckTroopSaver_1.get(2));
            String temp2 = GD.CR.getString(GD.deckTroopSaver_1.get(2));
            Dragboard db = cardSlot3.startDragAndDrop(TransferMode.COPY);
            db.setDragView(temp, e.getX(), e.getY());
            ClipboardContent cc = new ClipboardContent();
            cc.putString(temp2);
            db.setContent(cc);
        });
        cardSlot4.setOnDragDetected(e -> {
            Image temp = GD.CR.getDrag(GD.deckTroopSaver_1.get(3));
            String temp2 = GD.CR.getString(GD.deckTroopSaver_1.get(3));
            Dragboard db = cardSlot4.startDragAndDrop(TransferMode.COPY);
            db.setDragView(temp, e.getX(), e.getY());
            ClipboardContent cc = new ClipboardContent();
            cc.putString(temp2);
            db.setContent(cc);
        });

        pane.setOnDragOver(event -> {
            Dragboard db = event.getDragboard();
            event.acceptTransferModes(TransferMode.COPY);
            event.consume();
        });

        pane.setOnDragDropped((event) -> {
            Boolean droped = false;
            Dragboard db = event.getDragboard();
            BaseCard sample = troopFinder(db.getString(), (int) event.getX(), (int) event.getY());

            if (db.getString().equals("Zap") || db.getString().equals("Miner")) {
                GD.troopSaver_1.add(sample);
                Thread sampleTH = new Thread(GD.troopSaver_1.get(GD.troopSaver_1.size() - 1));
                sampleTH.start();
                droped = true;

            } else if (troopCanDeploy((int) event.getX(), (int) event.getY()) == true) {
                GD.troopSaver_1.add(sample);
                Thread sampleTH = new Thread(GD.troopSaver_1.get(GD.troopSaver_1.size() - 1));
                sampleTH.start();
                droped = true;
            }
            if (droped == true) {
                for (int i = 0; i < GD.deckTroopSaver_1.size(); i++) {
                    if (sample.getCardImage().equals(GD.deckTroopSaver_1.get(i))) {
                        Image temp = GD.deckTroopSaver_1.get(i);
                        GD.deckTroopSaver_1.remove(i);
                        GD.deckTroopSaver_1.add(temp);
                    }
                }
            }
        });
    }

    private void setTower() {

        King_1.setImage(GD.IR.King_1);
        King_2.setImage(GD.IR.King_2);
        Princess_1A.setImage(GD.IR.Princess_1);
        Princess_1B.setImage(GD.IR.Princess_1);
        Princess_1C.setImage(GD.IR.Princess_1);
        Princess_1D.setImage(GD.IR.Princess_1);
        Princess_2A.setImage(GD.IR.Princess_2);
        Princess_2B.setImage(GD.IR.Princess_2);
        Princess_2C.setImage(GD.IR.Princess_2);
        Princess_2D.setImage(GD.IR.Princess_2);

    }

    private Boolean troopCanDeploy(int x, int y) {

        if (x > 550 && x < 1350) {
            if (y > 530 && y < 930) {
                return true;
            }
        }
        return false;
    }

    private BaseCard troopFinder(String sample, int x, int y) {
        switch (sample) {
            case "RoyalGiant":
                return (new RoyalGiant(1, x, y, GD));
            case "MinionHorde":
                return new MinionHorde(1, x, y, GD);
            case "Zap":
                return new Zap(1, x, y, GD);
            case "Miner":
                return new Miner(1, x, y, GD);
            case "HogRider":
                return (new HogRider(1, x, y, GD));
            case "Valkyrie":
                return new Valkyrie(1, x, y, GD);
            case "ElixirCollector":
                return new ElixirCollector(1, x, y, GD);
            case "Balloon":
                return new Balloon(1, x, y, GD);
        }
        return null;
    }

}
