package GUI;

import Data.GameData;
import Threads.Menu.*;
import Threads.Menu.MenuMusic;
import javafx.fxml.FXML;
import javafx.scene.image.ImageView;
import java.io.IOException;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

public class MainMenuController {

    private GameData GD;

    private Thread BattleStartMusicTH;
    private BattleStartMusic battleStartMusicCL;
    private Thread menuMusicTH;
    private MenuMusic menuMusicCL;

    @FXML
    private ImageView background;
    @FXML
    private ImageView arenaIcon;
    @FXML
    private ImageView playWithFriend;
    @FXML
    private ImageView playWithSmartizie;
    @FXML
    private ImageView nextArena;
    @FXML
    private ImageView previousArena;
    @FXML
    private ImageView mainMenu_Wide;
    @FXML
    private ImageView aboutMenu;
    @FXML
    private ImageView deckMenu;
    @FXML
    private ImageView settingsMenu;
    @FXML
    private ImageView exit;

    @FXML
    private void playWithFriendClicked(MouseEvent event) throws IOException {

        GD.menuMediaPlayer.stop();
        battleStartMusicCL = new BattleStartMusic(GD);
        BattleStartMusicTH = new Thread(battleStartMusicCL);
        BattleStartMusicTH.start();

        FXMLLoader FXL = new FXMLLoader(getClass().getResource("/GUI/BattleField.fxml"));
        Parent root = FXL.load();
        BattleFieldController BFCTR = FXL.getController();
        BFCTR.setData(GD);
        BFCTR.init();

        Scene scene = new Scene(root);
        scene.getStylesheets().add(getClass().getResource("/Styles/BattleField.css").toExternalForm());

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        stage.setFullScreen(true);
    }

    @FXML
    private void playWithSmartizieClicked(MouseEvent event) {

    }

    @FXML
    private void nextArenaClicked(MouseEvent event) {
        if (GD.getArenaNumber() == 7) {
            GD.setArenaNumber(8);
            arenaIcon.setImage(GD.IR.arena8_Icon);
        } else if (GD.getArenaNumber() == 8) {
            GD.setArenaNumber(9);
            arenaIcon.setImage(GD.IR.arena9_Icon);
        }
    }

    @FXML
    private void previousArenaClicked(MouseEvent event) {
        if (GD.getArenaNumber() == 8) {
            GD.setArenaNumber(7);
            arenaIcon.setImage(GD.IR.arena7_Icon);
        } else if (GD.getArenaNumber() == 9) {
            GD.setArenaNumber(8);
            arenaIcon.setImage(GD.IR.arena8_Icon);
        }
    }

    @FXML
    private void deckMenuClicked(MouseEvent event) throws IOException {

        FXMLLoader FXL = new FXMLLoader(getClass().getResource("/GUI/DeckMenu.fxml"));
        Parent root = FXL.load();
        DeckMenuController DMCTR = FXL.getController();
        DMCTR.setData(GD);
        DMCTR.init();

        Scene scene = new Scene(root);

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.setScene(scene);
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        stage.setFullScreen(true);

    }

    @FXML
    private void aboutMenuClicked(MouseEvent event) {
//        try {
//            String aboutCSS = MQ.Check("AboutMenu");
//            Parent root = FXMLLoader.load(getClass().getResource("/GUI/AboutMenu.fxml"));
//            Scene scene = new Scene(root);
//            scene.getStylesheets().add(getClass().getResource(aboutCSS).toExternalForm());
//            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//            stage.setScene(scene);
//            stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
//            stage.setFullScreen(true);
//
//        } catch (IOException ex) {
//            Logger.getLogger(MainMenuController.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
    }

    @FXML
    private void settingsMenuClicked(MouseEvent event) {
//        try {
//            String settingsCSS = MQ.Check("SettingsMenu");
//            Parent root = FXMLLoader.load(getClass().getResource("/GUI/SettingsMenu.fxml"));
//            Scene scene = new Scene(root);
//            scene.getStylesheets().add(getClass().getResource(settingsCSS).toExternalForm());
//            Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
//            stage.setScene(scene);
//            stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
//            stage.setFullScreen(true);
//
//        } catch (IOException ex) {
//            Logger.getLogger(MainMenuController.class
//                    .getName()).log(Level.SEVERE, null, ex);
//        }
    }

    @FXML
    private void exitClicked(MouseEvent event) {

        Stage stage = (Stage) ((Node) event.getSource()).getScene().getWindow();
        stage.close();

    }

    public void setData(GameData GD) {
        this.GD = GD;
    }

    public void init() {
       
        setArenaIcon();
        setImages();

        if (GD.menuMusicState == false) {
            menuMusicCL = new MenuMusic(GD);
            menuMusicTH = new Thread(menuMusicCL);
            menuMusicTH.start();
            GD.menuMusicState = true;
        }

    }

    private void setImages() {

        background.setImage(GD.IR.background);
        playWithFriend.setImage(GD.IR.playWithFriend);
        playWithSmartizie.setImage(GD.IR.playWithSmartizie);
        nextArena.setImage(GD.IR.nextArena);
        previousArena.setImage(GD.IR.previousArena);
        deckMenu.setImage(GD.IR.deckMenu);
        mainMenu_Wide.setImage(GD.IR.mainMenu_Wide);
        aboutMenu.setImage(GD.IR.aboutMenu);
        settingsMenu.setImage(GD.IR.settingsMenu);
        exit.setImage(GD.IR.exit);

    }

    private void setArenaIcon() {
        if (GD.getArenaNumber() == 7) {
            arenaIcon.setImage(GD.IR.arena7_Icon);
        } else if (GD.getArenaNumber() == 8) {
            arenaIcon.setImage(GD.IR.arena8_Icon);
        } else if (GD.getArenaNumber() == 9) {
            arenaIcon.setImage(GD.IR.arena9_Icon);
        }
    }

}
