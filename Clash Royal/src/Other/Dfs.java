package Other;

public class Dfs {

    private String name;
    private int count;  
    
    public Dfs(String name,int count){
        setName(name);
        setCount(count);        
    }
    
    
    protected void setName(String name) {
        this.name = name;
    }
    protected void setCount(int count) {
        this.count = count;
    }    
    public String getName(){
        return name;
    }
    public int getCount(){
        return count;
    }
    

}
