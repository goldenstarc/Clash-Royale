package Main;

import java.io.FileInputStream;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.input.KeyCombination;
import javafx.stage.Stage;
import Data.GameData;

public class ClashRoyal extends Application {

    @Override
    public void start(Stage stage) throws Exception {

        GameData GD = new GameData();
        GD.setStage(stage);

 

        FXMLLoader FXL = new FXMLLoader(getClass().getResource("/GUI/SplashScreen.fxml"));
        Parent root = FXL.load();

        GUI.SplashScreenController SSCTR = FXL.getController();
        SSCTR.setData(GD);
        SSCTR.init();

        Scene scene = new Scene(root);

        stage.setTitle("Clash Royal");
        stage.setScene(scene);
        stage.setResizable(false);
        stage.setFullScreen(true);
        stage.getIcons().add(new Image(new FileInputStream("resources/Image/Icon/App_Icon.png")));
        stage.setFullScreenExitKeyCombination(KeyCombination.NO_MATCH);
        stage.show();

    }

    public static void main(String[] args) {

        Application.launch(args);
    }

}
