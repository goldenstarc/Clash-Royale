package Loader.TroopImage;

import java.io.File;
import javafx.scene.image.Image;

public final class CardResource {

    private final Resource troopRes;

    public CardResource() {
        troopRes = new Resource();
        setCard();
        setDrag();
    }

    public Image Balloon_Card;
    public Image DarkPrince_Card;
    public Image ElixirCollector_Card;
    public Image HogRider_Card;
    public Image IceWizard_Card;
    public Image InfernoTower_Card;
    public Image LavaHound_Card;
    public Image Miner_Card;
    public Image MinionHorde_Card;
    public Image Rage_Card;
    public Image RoyalGiant_Card;
    public Image Valkyrie_Card;
    public Image Zap_Card;

    private void setCard() {

        Balloon_Card = new Image((new File(troopRes.getResourceString("Balloon" + "_Card")).toURI().toString()));
        DarkPrince_Card = new Image((new File(troopRes.getResourceString("DarkPrince" + "_Card")).toURI().toString()));
        ElixirCollector_Card = new Image((new File(troopRes.getResourceString("ElixirCollector" + "_Card")).toURI().toString()));
        HogRider_Card = new Image((new File(troopRes.getResourceString("HogRider" + "_Card")).toURI().toString()));
        IceWizard_Card = new Image((new File(troopRes.getResourceString("IceWizard" + "_Card")).toURI().toString()));
        InfernoTower_Card = new Image((new File(troopRes.getResourceString("InfernoTower" + "_Card")).toURI().toString()));
        LavaHound_Card = new Image((new File(troopRes.getResourceString("LavaHound" + "_Card")).toURI().toString()));
        Miner_Card = new Image((new File(troopRes.getResourceString("Miner" + "_Card")).toURI().toString()));
        MinionHorde_Card = new Image((new File(troopRes.getResourceString("MinionHorde" + "_Card")).toURI().toString()));;
        Rage_Card = new Image((new File(troopRes.getResourceString("Rage" + "_Card")).toURI().toString()));
        RoyalGiant_Card = new Image((new File(troopRes.getResourceString("RoyalGiant" + "_Card")).toURI().toString()));
        Valkyrie_Card = new Image((new File(troopRes.getResourceString("Valkyrie" + "_Card")).toURI().toString()));
        Zap_Card = new Image((new File(troopRes.getResourceString("Zap" + "_Card")).toURI().toString()));

    }

    public Image Balloon_Drag;
    public Image DarkPrince_Drag;
    public Image ElixirCollector_Drag;
    public Image HogRider_Drag;
    public Image IceWizard_Drag;
    public Image InfernoTower_Drag;
    public Image LavaHound_Drag;
    public Image Miner_Drag;
    public Image MinionHorde_Drag;
    public Image Rage_Drag;
    public Image RoyalGiant_Drag;
    public Image Valkyrie_Drag;
    public Image Zap_Drag;

    private void setDrag() {

        Balloon_Drag = new Image((new File(troopRes.getResourceString("Balloon" + "_Drag")).toURI().toString()));
        DarkPrince_Drag = new Image((new File(troopRes.getResourceString("DarkPrince" + "_Drag")).toURI().toString()));
        ElixirCollector_Drag = new Image((new File(troopRes.getResourceString("ElixirCollector" + "_Drag")).toURI().toString()));
        HogRider_Drag = new Image((new File(troopRes.getResourceString("HogRider" + "_Drag")).toURI().toString()));
        IceWizard_Drag = new Image((new File(troopRes.getResourceString("IceWizard" + "_Drag")).toURI().toString()));
        InfernoTower_Drag = new Image((new File(troopRes.getResourceString("InfernoTower" + "_Drag")).toURI().toString()));
        LavaHound_Drag = new Image((new File(troopRes.getResourceString("LavaHound" + "_Drag")).toURI().toString()));
        Miner_Drag = new Image((new File(troopRes.getResourceString("Miner" + "_Drag")).toURI().toString()));
        MinionHorde_Drag = new Image((new File(troopRes.getResourceString("MinionHorde" + "_Drag")).toURI().toString()));;
        Rage_Drag = new Image((new File(troopRes.getResourceString("Rage" + "_Drag")).toURI().toString()));
        RoyalGiant_Drag = new Image(new File(troopRes.getResourceString("RoyalGiant" + "_Drag")).toURI().toString());
        Valkyrie_Drag = new Image((new File(troopRes.getResourceString("Valkyrie" + "_Drag")).toURI().toString()));
        Zap_Drag = new Image((new File(troopRes.getResourceString("Zap" + "_Drag")).toURI().toString()));
    }

    public Image getDrag(Image get) {
        if (get.equals(RoyalGiant_Card)) {
            return RoyalGiant_Drag;
        }
        if (get.equals(MinionHorde_Card)) {
            return MinionHorde_Drag;
        }
        if (get.equals(Zap_Card)) {
            return Zap_Drag;
        }
        if (get.equals(Miner_Card)) {
            return Miner_Drag;
        }
        if (get.equals(HogRider_Card)) {
            return HogRider_Drag;
        }
        if (get.equals(Valkyrie_Card)) {
            return Valkyrie_Drag;
        }
        if (get.equals(ElixirCollector_Card)) {
            return ElixirCollector_Drag;
        }
        if (get.equals(Balloon_Card)) {
            return Balloon_Drag;
        }
        return null;
    }

    public String getString(Image get) {
        if (get.equals(RoyalGiant_Card)) {
            return "RoyalGiant";
        }
        if (get.equals(MinionHorde_Card)) {
            return "MinionHorde";
        }
        if (get.equals(Zap_Card)) {
            return "Zap";
        }
        if (get.equals(Miner_Card)) {
            return "Miner";
        }
        if (get.equals(HogRider_Card)) {
            return "HogRider";
        }
        if (get.equals(Valkyrie_Card)) {
            return "Valkyrie";
        }
        if (get.equals(ElixirCollector_Card)) {
            return "ElixirCollector";
        }
        if (get.equals(Balloon_Card)) {
            return "Balloon";
        }
        return null;
    }

}
