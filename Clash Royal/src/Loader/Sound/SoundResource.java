package Loader.Sound;

import java.io.File;
import javafx.scene.media.Media;

public final class SoundResource {

    private final Resource soundRes;

    public SoundResource() {
        soundRes = new Resource();
        musicLoader();
        sfxLoader();
        countingLoader();
        jingleLoader();
    }

    public Media BattleMusic_1;
    public Media BattleMusic_2;
    public Media BattleMusic_3;

    public Media MenuMusic_1;
    public Media MenuMusic_2;
    public Media MenuMusic_3;

    public Media Sudden_death;
    public Media Pre_result;
    public Media Win;
    public Media Lose;
    public Media Draw;
    public Media Post_result_loop;
    public Media loop_60sec;
    public Media loop_30sec;

    private void musicLoader() {

        BattleMusic_1 = new Media(new File(soundRes.getResourceString("BattleMusic_1")).toURI().toString());
        BattleMusic_2 = new Media(new File(soundRes.getResourceString("BattleMusic_2")).toURI().toString());
        BattleMusic_3 = new Media(new File(soundRes.getResourceString("BattleMusic_3")).toURI().toString());

        MenuMusic_1 = new Media(new File(soundRes.getResourceString("MenuMusic_1")).toURI().toString());
        MenuMusic_2 = new Media(new File(soundRes.getResourceString("MenuMusic_2")).toURI().toString());
        MenuMusic_3 = new Media(new File(soundRes.getResourceString("MenuMusic_3")).toURI().toString());

        Sudden_death = new Media(new File(soundRes.getResourceString("Sudden_death")).toURI().toString());
        Pre_result = new Media(new File(soundRes.getResourceString("Pre_result")).toURI().toString());
        Win = new Media(new File(soundRes.getResourceString("Win")).toURI().toString());
        Lose = new Media(new File(soundRes.getResourceString("Lose")).toURI().toString());
        Draw = new Media(new File(soundRes.getResourceString("Draw")).toURI().toString());
        Post_result_loop = new Media(new File(soundRes.getResourceString("Post_result_loop")).toURI().toString());
        loop_60sec = new Media(new File(soundRes.getResourceString("loop_60sec")).toURI().toString());
        loop_30sec = new Media(new File(soundRes.getResourceString("loop_30sec")).toURI().toString());
    }

    public Media SplashMusic;

    private void sfxLoader() {
        SplashMusic = new Media(new File(soundRes.getResourceString("SplashMusic")).toURI().toString());

    }

    public Media arctic_arena_jingle;
    public Media barbarian_jingle_01;
    public Media bone_pit_arena_jingle_01;
    public Media dark_arena_jingle_02;
    public Media extratime_jingle_02;
    public Media goblin_arena_jingle_01;
    public Media jungle_arena_jingle_01;
    public Media legendary_arena_jingle_01;
    public Media tut_jing_01;

    private void jingleLoader() {

        arctic_arena_jingle = new Media(new File(soundRes.getResourceString("arctic_arena_jingle")).toURI().toString());
        barbarian_jingle_01 = new Media(new File(soundRes.getResourceString("barbarian_jingle_01")).toURI().toString());
        bone_pit_arena_jingle_01 = new Media(new File(soundRes.getResourceString("bone_pit_arena_jingle_01")).toURI().toString());
        dark_arena_jingle_02 = new Media(new File(soundRes.getResourceString("dark_arena_jingle_02")).toURI().toString());
        extratime_jingle_02 = new Media(new File(soundRes.getResourceString("extratime_jingle_02")).toURI().toString());
        goblin_arena_jingle_01 = new Media(new File(soundRes.getResourceString("goblin_arena_jingle_01")).toURI().toString());
        jungle_arena_jingle_01 = new Media(new File(soundRes.getResourceString("jungle_arena_jingle_01")).toURI().toString());
        legendary_arena_jingle_01 = new Media(new File(soundRes.getResourceString("legendary_arena_jingle_01")).toURI().toString());
        tut_jing_01 = new Media(new File(soundRes.getResourceString("tut_jing_01")).toURI().toString());
    }

    public Media CD_1;
    public Media CD_2;
    public Media CD_3;
    public Media CD_4;
    public Media CD_5;
    public Media CD_6;
    public Media CD_7;
    public Media CD_8;
    public Media CD_9;
    public Media CD_10;
    public Media Crown_1st;
    public Media Crown_2nd;
    public Media Crown_3rd;

    private void countingLoader() {

        CD_1 = new Media(new File(soundRes.getResourceString("1_CD")).toURI().toString());
        CD_2 = new Media(new File(soundRes.getResourceString("2_CD")).toURI().toString());
        CD_3 = new Media(new File(soundRes.getResourceString("3_CD")).toURI().toString());
        CD_4 = new Media(new File(soundRes.getResourceString("4_CD")).toURI().toString());
        CD_5 = new Media(new File(soundRes.getResourceString("5_CD")).toURI().toString());
        CD_6 = new Media(new File(soundRes.getResourceString("6_CD")).toURI().toString());
        CD_7 = new Media(new File(soundRes.getResourceString("7_CD")).toURI().toString());
        CD_8 = new Media(new File(soundRes.getResourceString("8_CD")).toURI().toString());
        CD_9 = new Media(new File(soundRes.getResourceString("9_CD")).toURI().toString());
        CD_10 = new Media(new File(soundRes.getResourceString("10_CD")).toURI().toString());
        Crown_1st = new Media(new File(soundRes.getResourceString("1st_Crown")).toURI().toString());
        Crown_2nd = new Media(new File(soundRes.getResourceString("2nd_Crown")).toURI().toString());
        Crown_3rd = new Media(new File(soundRes.getResourceString("3rd_Crown")).toURI().toString());

    }
}
