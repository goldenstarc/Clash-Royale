package Loader.BaseImage;

import java.io.File;
import javafx.scene.image.Image;

public final class ImageResource {

    private final Resource imageRes;

    public ImageResource() {
        imageRes = new Resource();
        setMainMenuImages();
        setCommonMenuImages();
        setDeckMenuImages();
        setArenaBackground();
        setElixirBar();
        setAboutMenuImages();
        setSettingsMenuImages();
        setTowers();
    }
    //  ***************************************************************************************************/
    //                                        Common Menu Images
    // ****************************************************************************************************/
    public Image background;
    public Image mainMenu;
    public Image deckMenu;
    public Image aboutMenu;
    public Image SplashScreen;

    private void setCommonMenuImages() {

        SplashScreen = new Image(new File(imageRes.getResourceString("SplashScreen")).toURI().toString());
        background = new Image((new File(imageRes.getResourceString("Background")).toURI().toString()));
        mainMenu = new Image((new File(imageRes.getResourceString("MainMenu")).toURI().toString()));
        aboutMenu = new Image((new File(imageRes.getResourceString("AboutMenu")).toURI().toString()));
        deckMenu = new Image((new File(imageRes.getResourceString("DeckMenu")).toURI().toString()));
        exit = new Image((new File(imageRes.getResourceString("Exit")).toURI().toString()));

    }

    //  ***************************************************************************************************/
    //                                            Main Menu
    // ****************************************************************************************************/
    public Image arena7_Icon;
    public Image arena8_Icon;
    public Image arena9_Icon;
    public Image playWithFriend;
    public Image playWithSmartizie;
    public Image mainMenu_Wide;
    public Image nextArena;
    public Image previousArena;
    public Image settingsMenu;
    public Image exit;

    private void setMainMenuImages() {

        arena7_Icon = new Image((new File(imageRes.getResourceString("Arena7_Gif")).toURI().toString()));
        arena8_Icon = new Image((new File(imageRes.getResourceString("Arena8_Gif")).toURI().toString()));
        arena9_Icon = new Image((new File(imageRes.getResourceString("Arena9_Gif")).toURI().toString()));
        playWithFriend = new Image((new File(imageRes.getResourceString("PlayWithFriend")).toURI().toString()));
        playWithSmartizie = new Image((new File(imageRes.getResourceString("PlayWithSmartizie")).toURI().toString()));
        mainMenu_Wide = new Image((new File(imageRes.getResourceString("MainMenu_Wide")).toURI().toString()));
        nextArena = new Image((new File(imageRes.getResourceString("NextArena")).toURI().toString()));
        previousArena = new Image((new File(imageRes.getResourceString("PreviousArena")).toURI().toString()));
        settingsMenu = new Image((new File(imageRes.getResourceString("SettingsMenu")).toURI().toString()));

    }

    //  ***************************************************************************************************/
    //                                           Deck Menu
    // ****************************************************************************************************/
    public Image woodSlot;
    public Image deckMenu_Wide;
    public Image battleDeck;

    private void setDeckMenuImages() {

        woodSlot = new Image((new File(imageRes.getResourceString("WoodSlot")).toURI().toString()));
        deckMenu_Wide = new Image((new File(imageRes.getResourceString("DeckMenu_Wide")).toURI().toString()));
        battleDeck = new Image((new File(imageRes.getResourceString("BattleDeck")).toURI().toString()));

    }

    //  ***************************************************************************************************/
    //                                           About Menu
    // ****************************************************************************************************/
    private void setAboutMenuImages() {

    }

    //  ***************************************************************************************************/
    //                                          Settings Menu
    // ****************************************************************************************************/
    private void setSettingsMenuImages() {

    }

    //  ***************************************************************************************************/
    //                                             Arenas
    // ****************************************************************************************************/
    public Image arena7_Background;
    public Image arena8_Background;
    public Image arena9_Background;

    private void setArenaBackground() {

        arena7_Background = new Image((new File(imageRes.getResourceString("Arena7_Background")).toURI().toString()));
        arena8_Background = new Image((new File(imageRes.getResourceString("Arena8_Background")).toURI().toString()));
        arena9_Background = new Image((new File(imageRes.getResourceString("Arena9_Background")).toURI().toString()));

    }

    //  ***************************************************************************************************/
    //                                           Elixir Bar
    // ****************************************************************************************************/
    public Image Elixir_Card;
    public Image Elixir_0;
    public Image Elixir_1;
    public Image Elixir_2;
    public Image Elixir_3;
    public Image Elixir_4;
    public Image Elixir_5;
    public Image Elixir_6;
    public Image Elixir_7;
    public Image Elixir_8;
    public Image Elixir_9;
    public Image Elixir_10;

    private void setElixirBar() {
        Elixir_Card = new Image((new File(imageRes.getResourceString("Elixir_Card")).toURI().toString()));
        Elixir_0 = new Image((new File(imageRes.getResourceString("Elixir_0")).toURI().toString()));
        Elixir_1 = new Image((new File(imageRes.getResourceString("Elixir_1")).toURI().toString()));
        Elixir_2 = new Image((new File(imageRes.getResourceString("Elixir_2")).toURI().toString()));
        Elixir_3 = new Image((new File(imageRes.getResourceString("Elixir_3")).toURI().toString()));
        Elixir_4 = new Image((new File(imageRes.getResourceString("Elixir_4")).toURI().toString()));
        Elixir_5 = new Image((new File(imageRes.getResourceString("Elixir_5")).toURI().toString()));
        Elixir_6 = new Image((new File(imageRes.getResourceString("Elixir_6")).toURI().toString()));
        Elixir_7 = new Image((new File(imageRes.getResourceString("Elixir_7")).toURI().toString()));
        Elixir_8 = new Image((new File(imageRes.getResourceString("Elixir_8")).toURI().toString()));
        Elixir_9 = new Image((new File(imageRes.getResourceString("Elixir_9")).toURI().toString()));
        Elixir_10 = new Image((new File(imageRes.getResourceString("Elixir_10")).toURI().toString()));
    }

    //  ***************************************************************************************************/
    //                                           Towers
    // ****************************************************************************************************/
    public Image King_1;
    public Image King_2;
    public Image Princess_1;
    public Image Princess_2;

    private void setTowers() {

        King_1 = new Image((new File(imageRes.getResourceString("King_1")).toURI().toString()));
        King_2 = new Image((new File(imageRes.getResourceString("King_2")).toURI().toString()));
        Princess_1 = new Image((new File(imageRes.getResourceString("Princess_1")).toURI().toString()));
        Princess_2 = new Image((new File(imageRes.getResourceString("Princess_2")).toURI().toString()));
    }

}
