package Utilities;

import java.awt.Dimension;
import java.awt.Toolkit;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

public class ResolutionCheck {

    public static String getResolution() {

        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        double width = screenSize.getWidth();
        double height = screenSize.getHeight();
       
        if (width == 3840 && height == 2160) {
            return "UHD";
        } else if (width == 2560 && height == 1440) {
            return "QHD";
        } else if (width == 1920 && height == 1080) {
            return "FHD";
        }
        return null;
    }
}
