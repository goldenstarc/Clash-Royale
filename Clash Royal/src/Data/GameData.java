package Data;

import Card.Building.Defensive.PrincessTower;
import Card.Building.Defensive.KingTower;
import Card.BaseCard;
import Loader.BaseImage.ImageResource;
import Loader.TroopImage.CardResource;
import Loader.Sound.SoundResource;
import java.util.ArrayList;
import javafx.scene.image.Image;
import javafx.scene.layout.Pane;
import javafx.scene.media.MediaPlayer;
import javafx.stage.Stage;

public class GameData {

    private int arenaNumber = 7;
    public final int cardNumber = 5;
    public final int freeCardNumber = 8;
    public final int arenaTowerNumber = 4;
    private int elixirCount = 5;
    public int time = 150;

    public ImageResource IR = new ImageResource();
    public CardResource CR = new CardResource();
    public SoundResource SR = new SoundResource();

    public ArrayList<Image> deckTroopSaver_1 = new ArrayList<>();
    public ArrayList<Image> deckTroopSaver_2 = new ArrayList<>();
    public ArrayList<BaseCard> troopSaver_1 = new ArrayList<>();
    public ArrayList<BaseCard> troopSaver_2 = new ArrayList<>();
    public ArrayList<BaseCard> freeCardSaver = new ArrayList<>();

    public KingTower king_1;
    public KingTower king_2;
    public PrincessTower princess_1A;
    public PrincessTower princess_1B;
    public PrincessTower princess_1C;
    public PrincessTower princess_1D;
    public PrincessTower princess_2A;
    public PrincessTower princess_2B;
    public PrincessTower princess_2C;
    public PrincessTower princess_2D;

    public MediaPlayer menuMediaPlayer;
    public Stage stage;
    public Pane pane;

    public Boolean musicState = true;
    public Boolean sfxState = true;
    public Boolean started = false;
    public Boolean menuMusicState = false;

    public int earnedCup1;
    public int earnedCup2;

    public GameData() {

        initTowers();
        initCards();

    }

    private void initCards() {

        deckTroopSaver_1.add(CR.Zap_Card);
//        deckTroopSaver_1.add(CR.MinionHorde_Card);
        deckTroopSaver_1.add(CR.RoyalGiant_Card);
        deckTroopSaver_1.add(CR.HogRider_Card);
 //       deckTroopSaver_1.add(CR.Miner_Card);
 //       deckTroopSaver_1.add(CR.Valkyrie_Card);
        deckTroopSaver_1.add(CR.ElixirCollector_Card);
        deckTroopSaver_1.add(CR.Balloon_Card);

    }

    private void initTowers() {

        king_1 = new KingTower(1, 961, 885, this);
        princess_1A = new PrincessTower(1, 619, 807, this);
        princess_1B = new PrincessTower(1, 796, 769, this);
        princess_1C = new PrincessTower(1, 1124, 809, this);
        princess_1D = new PrincessTower(1, 1301, 809, this);

        troopSaver_1.add(king_1);
        troopSaver_1.add(princess_1A);
        troopSaver_1.add(princess_1B);
        troopSaver_1.add(princess_1C);
        troopSaver_1.add(princess_1D);

        king_2 = new KingTower(2, 961, 146, this);
        princess_2A = new PrincessTower(2, 619, 236, this);
        princess_2B = new PrincessTower(2, 796, 236, this);
        princess_2C = new PrincessTower(2, 1124, 236, this);
        princess_2D = new PrincessTower(2, 1301, 236, this);

        troopSaver_2.add(king_2);
        troopSaver_2.add(princess_2A);
        troopSaver_2.add(princess_2B);
        troopSaver_2.add(princess_2C);
        troopSaver_2.add(princess_2D);

    }

    public int getArenaNumber() {
        return arenaNumber;
    }

    public void setArenaNumber(int arenaNumber) {
        this.arenaNumber = arenaNumber;
    }

    public synchronized void changeElixirCount(int t) {
        if (elixirCount < 10) {
            elixirCount += t;
        }
    }

    public int getElixirCount() {
        return elixirCount;
    }

    public void setStage(Stage stage) {
        this.stage = stage;
    }

    public void setPane(Pane pane) {
        this.pane = pane;
    }
}
